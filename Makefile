DATE=`date +%Y-%m-%d---%H-%M-%S`
PACKAGE="latex-shortmathj"

ctan:
	mkdir -p dist/$(PACKAGE)
	rm -rf dist/$(PACKAGE)/*
	cp -t dist/$(PACKAGE) *.tex *.sty *.bib *.pdf *.md *.js *.json
	cd dist; zip -r $(PACKAGE)-$(DATE).zip $(PACKAGE)
